;汇编器采用nasm 编译方法 nasm boot.asm -o boot.bin  然后讲boot.bin 的内容复制到软盘第一个扇区
	org 0x7c00
	jmp BEGIN
	cache equ 0x8c00
BEGIN:	
	;软驱复位
	mov ah,0x0
	mov dl,0x0
	int 0x13
	
	;读入一个扇区 现在要读两个扇区
	mov ah,0x02
	mov al,0x01
	mov ch,0x0
	mov cl,0x1
	mov dh,0x0
	mov dl,0x0
	mov bx,cache
	int 0x13
	nop
	
	;设置显示模式 为图形模式 
	mov ah,0x00
	mov al,0x05
	int 0x10
	
	;设置调色盘
	mov ah,0xb
	mov bh,0x0
	mov bl,0x4
	int 0x10
	
	;等待键盘输入
	mov bx,0x0
input:
	mov ah,0x0
	int 0x16
	cmp ax,0xe08 ;删除键 光标会自动向前一格
	jz .2 
.1:
	mov bl,[index]
	mov  [cmd+bx],al ;al 是中断返回的字符
	add bl,1
	mov [index],bl
	jmp .3
.2:
	mov bl,[index]
	mov ax,0x20 ;显示空格 因为没有清屏，所以就算清楚了字符，显示过的内容依然会在屏幕上
	sub bl,1
	js .4
.5:
	mov [cmd+bx],ax
	mov [index],bl
.3:
	call printStr
	jmp input
.4:
	call resetCursor 
	mov bl,0x0
	jmp .5

printStr:
	call resetCursor
	call clearScreen
	call resetCursor
	
	mov al,[index]
	cmp al,0x0 ;如果长度为0 直接返回原地等待
	jz input
	mov cl,al ;循环数 字符串的长度
	mov si,0
p:
	mov bx,cmd ;开始地址
	mov al,[bx+si]
	add si,1
	call printChar
	loop p
	ret
;调用的方法就是先把字符输进al 然后调用
printChar:
	mov ah,0x0e
	mov bh,0x0
	mov bl,0x31
	int 0x10
	ret
;循环100次用空格清屏
clearScreen:
	mov cx,100 
.1:
	mov al,0x20
	call printChar
	loop .1
	ret
;光标移复位
resetCursor:
	mov ah,0x02
	mov dh,0x0
	mov dl,0x0
	int 0x10
	ret

exitprogram:
	hlt
DATA:
	index db 0x0
	cmd     db ""